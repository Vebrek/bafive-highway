CREATE DATABASE bafive;
use bafive;

CREATE TABLE `cc_usage` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `age` int unsigned NOT NULL,
  `street` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(50) NOT NULL,
  `latitude` decimal(16, 12) NOT NULL,
  `longitude` decimal(16, 12) NOT NULL,
  `ccnumber` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;