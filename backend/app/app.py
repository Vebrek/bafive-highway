from flask import Flask, Response, jsonify, json, request
from flask_cors import CORS
import mysql.connector
import decimal


app = Flask(__name__)
CORS(app)


class JsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, decimal.Decimal):
            return float(obj)


def get_config():
    return {
        'user': 'root',
        'password': 'root',
        'host': 'db',
        'port': '3306',
        'database': 'bafive'
    }


def create_insert_query(raw_values):
    input_types = {
        'first_name': str,
        'last_name': str,
        'age': int,
        'street': str,
        'city': str,
        'state': str,
        'latitude': float,
        'longitude': float,
        'ccnumber': int,
    }
    print(raw_values)
    values = [str(raw_values.get(key, None)) for key, cast_to in input_types.items()]

    sql = f"""INSERT INTO cc_usage({', '.join(input_types.keys())}) VALUES('{"', '".join(values)}')"""
    return sql


def create_insert_query_many(raw_values):
    input_types = {
        'first_name': str,
        'last_name': str,
        'age': int,
        'street': str,
        'city': str,
        'state': str,
        'latitude': float,
        'longitude': float,
        'ccnumber': int,
    }

    sql = f"""INSERT INTO cc_usage 
        ({', '.join(input_types.keys())}) 
        VALUES({", ".join(['%s' for _ in input_types.keys()])})
    """

    cleaned_values = []
    for row in raw_values:
        cleaned_values.append(tuple([str(row.get(key, None)) for key in input_types.keys()]))

    return sql, cleaned_values


def insert(sql, values=None):
    try:
        config = get_config()
        connection = mysql.connector.connect(**config)
        cursor = connection.cursor()
        if values is None:
            cursor.execute(sql)
        else:
            cursor.executemany(sql, values)
        connection.commit()

        cursor.close()
        connection.close()
        return {'status': 201}

    except mysql.connector.Error as err:
        print("Something went wrong: {}".format(err))
        return {'status': 500, 'message': "Something went wrong: {}".format(err)}


@app.route('/')
def index():
    return 'Pong'


@app.route('/insert_one', methods=['POST'])
def insert_one():
    if request.method == 'POST':
        new_data = json.loads(request.data)
        sql = create_insert_query(new_data)
        response = insert(sql)

        return Response(response.get('message'), status=response.get('status'), mimetype='application/json')


@app.route('/insert_many', methods=['POST'])
def insert_many():
    if request.method == 'POST':
        new_data = json.loads(request.data)

        sql, values = create_insert_query_many(new_data)
        config = get_config()
        connection = mysql.connector.connect(**config)
        cursor = connection.cursor()

        cursor.executemany(sql, values)
        connection.commit()

        cursor.close()
        connection.close()

        return Response(
            f"Sucsessfully added {len(new_data)} row{'s' if len(new_data) > 0 else ''}",
            status=201,
            mimetype='application/json'
        )


@app.route('/find', methods=['POST'])
def find():
    query = 'SELECT * FROM cc_usage '
    if request.data:
        body = json.loads(request.data)

        if body.get('query'):
            query += 'WHERE ' + str(body.get('query'))
        if body.get('limit', 0) == 0:
            query += ' LIMIT 1000'
        else:
            query += ' LIMIT ' + str(max(int(body.get('limit', 1000)), 0))
    else:
        query += 'LIMIT 1000'
    query += ';'

    config = get_config()
    connection = mysql.connector.connect(**config)
    cursor = connection.cursor()
    # cursor.execute('SELECT * FROM cc_usage LIMIT 5000')
    cursor.execute(query)

    keys = ['seq', 'first_name', 'last_name', 'age', 'street', 'city', 'state', 'latitude', 'longitude', 'ccnumber']
    results = [{key: val for (key, val) in zip(keys, row)} for row in cursor]
    cursor.close()
    connection.close()

    app.json_encoder = JsonEncoder
    return jsonify(results)


@app.route('/delete_all')
def delete_all():
    try:
        config = get_config()
        connection = mysql.connector.connect(**config)
        cursor = connection.cursor()
        cursor.execute("DELETE FROM cc_usage")

        connection.commit()

        cursor.close()
        connection.close()

        return 'Deleted all'

    except mysql.connector.Error as err:
        return f'Failed to delete... {err}'


if __name__ == '__main__':
    app.run(host='0.0.0.0')
