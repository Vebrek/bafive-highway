import React, { useState } from 'react';
import { Button, Grid, Input, TextField } from '@mui/material';
import DataTable from '../components/data_table';


const Search = () => {
  const [query, setQuery] = useState('');
  const [limit, setLimit] = useState(0);
  const [data, setData] = useState([])
  const [ isLoading, setIsLoading ] = useState(false);

  const search = async () => {
    setIsLoading(true);
    const data = {
      "query": query,
      "limit": limit,
    }

    const response = await fetch('http://localhost:5000/find', {
      method: 'POST',
      // mode: 'no-cors', // no-cors, *cors, same-origin
      headers: {
          'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).catch((error) => {
      console.log(error)
    }).finally(() => {
      setIsLoading(false);
    });
    const results = await response.json();
    console.log(results);
    setData(results);
    
  };
  
  return (
    <Grid container spacing={2} alignItems="flex-end">
      <Grid item xs={6}>
        <TextField 
          label="Query" 
          variant="standard" 
          value={query} 
          onChange={ev => setQuery(ev.target.value)} 
          fullWidth 
        />
      </Grid>  
      <Grid item xs={1}>
        <TextField 
          type="number" 
          label="Limit" 
          variant="standard" 
          fullWidth 
          value={limit} 
          onChange={ev => setLimit(ev.target.value, 0)}
        />
      </Grid>
      <Grid item xs={2}>
      <Button variant="contained" color="primary" onClick={() => search()}>Search</Button>
      </Grid>
      <Grid item xs={12}>
        <DataTable data={data} isLoading={isLoading} />
      </Grid>
    </Grid>
  );
};
  
export default Search;