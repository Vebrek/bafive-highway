import React, { useState, useEffect } from 'react';
import AddData from '../components/add-data';
import DataTable from '../components/data_table';
import { Grid } from '@mui/material';
import DataOptions from '../components/data-options';


const Home = () => {
  const [ data, setData ] = useState([]);
  const [ isLoading, setIsLoading ] = useState(true);

  const loadData = async () => {
    const response = await fetch('http://127.0.0.1:5000/find', {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json'
      },
    });
    const results = await response.json();
    setData(results);
    setIsLoading(false);
  };

  useEffect(() => {
    loadData();
  }, [])

  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <AddData />
        </Grid>
        <Grid item xs={12}>
          <DataTable data={data} isLoading={isLoading} />
        </Grid>
        <Grid item xs={12}>
          <DataOptions />
        </Grid>
      </Grid>
    </>
  );
};
  
  export default Home;