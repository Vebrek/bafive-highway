import { Button, Grid, TextField } from '@mui/material';
import React from 'react';
import { useState } from 'react';

const AddData = () => {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [age, setAge] = useState('');
    const [street, setStreet] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');
    const [latitude, setLatitude] = useState('');
    const [longitude, setLongitude] = useState('');
    const [ccnumber, setCCNumber] = useState('');

    const sendData = async () => {
        const data = {
            "first_name": firstName,
            "last_name": lastName,
            "age": age,
            "street": street,
            "city": city,
            "state": state,
            "latitude": latitude,
            "longitude": longitude,
            "ccnumber": ccnumber
        }

        const response = await fetch('http://127.0.0.1:5000/insert_one', {
            method: 'POST',
            // mode: 'no-cors', // no-cors, *cors, same-origin
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        });
        
        setFirstName('');
        setLastName('');
        setAge('');
        setStreet('');
        setCity('');
        setState('');
        setLatitude('');
        setLongitude('');
        setCCNumber('');
    }
    
    return (
        <Grid container direction="row" justifyContent="space-between" alignItems="center">
            <Grid item xs={1}>
                <TextField id="outlined-basic" size="small" label="Firstname" variant="outlined" value={firstName} onChange={ev => setFirstName(ev.target.value)} />
            </Grid>
            <Grid item xs={1}>
                <TextField id="outlined-basic" size="small" label="Lastname" variant="outlined" value={lastName} onChange={ev => setLastName(ev.target.value)}/>
            </Grid>
            <Grid item xs={1}>
                <TextField id="outlined-basic" size="small" label="Age" variant="outlined" value={age} onChange={ev => setAge(ev.target.value)}/>
            </Grid>
            <Grid item xs={1}>
                <TextField id="outlined-basic" size="small" label="Street" variant="outlined" value={street} onChange={ev => setStreet(ev.target.value)} />
            </Grid>
            <Grid item xs={1}>
                <TextField id="outlined-basic" size="small" label="City" variant="outlined" value={city} onChange={ev => setCity(ev.target.value)} />
            </Grid>
            <Grid item xs={1}>
                <TextField id="outlined-basic" size="small" label="State" variant="outlined" value={state} onChange={ev => setState(ev.target.value)} />
            </Grid>
            <Grid item xs={1}>
                <TextField id="outlined-basic" size="small" label="Latitude" variant="outlined" value={latitude} onChange={ev => setLatitude(ev.target.value)} />
            </Grid>
            <Grid item xs={1}>
                <TextField id="outlined-basic" size="small" label="Longitude" variant="outlined" value={longitude} onChange={ev => setLongitude(ev.target.value)} />
            </Grid>
            <Grid item xs={2}>
                <TextField id="outlined-basic" size="small" label="CC number" variant="outlined" value={ccnumber} onChange={ev => setCCNumber(ev.target.value)} />
            </Grid>
            <Grid item xs={1}>
                <Button  variant="contained" onClick={() => sendData()}>Save</Button>
            </Grid>
        </Grid>
    )
}

export default AddData;