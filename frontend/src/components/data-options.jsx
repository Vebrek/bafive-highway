import * as React from 'react';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { Button, Fab } from '@mui/material';
import MoreVertIcon from '@mui/icons-material/MoreVert';


const Papa = require('papaparse');


const DataOptions = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [alertOpen, setAlertOpen] = React.useState(false);
  
  const style = {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
    position: 'fixed',
  };

  const open = Boolean(anchorEl);
  const handleClickListItem = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const insertMany = async (data) => {
    let buffer = 10000;
    for(let i = 0; i <= data.length; i += buffer){
      await fetch('http://127.0.0.1:5000/insert_many', {
        method: 'POST',
        body: JSON.stringify(data.slice(i, i + buffer)) // body data type must match "Content-Type" header
      });
    }
  }

  const insertFromFile = () => {
    handleClose()
    Papa.parse('../45784.csv', {
      header: true,
      download: true,
      dynamicTyping: true,
      complete: function(results) {
        insertMany(results.data);
      }
    });
  };

  const deleteAll = async () => {
    await fetch('http://localhost:5000/delete_all');
    setAlertOpen(false)
  };


  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <Fab color="primary" aria-label="add" style={style} onClick={handleClickListItem}><MoreVertIcon /></Fab>
      <Menu
        id="lock-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'lock-button',
          role: 'listbox',
        }}
      >
        <MenuItem onClick={() => insertFromFile()}>
            Add from file
        </MenuItem>
        <MenuItem onClick={() => setAlertOpen(true)}>
            Delete all
        </MenuItem>
      </Menu>
      <Dialog
        open={alertOpen}
        onClose={() => setAlertOpen(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          Are you sure?
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            You are about to delete all data and it is not possible to recover it after it is deleted.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setAlertOpen(false)} autoFocus>Cancel</Button>
          <Button variant="outlined" color="error" onClick={() => deleteAll()}>Continue</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default DataOptions;