import React, { useEffect } from 'react';
import { CircularProgress, Grid } from '@mui/material';
import { BarChart, Brush, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';



const AgeGraph = () => {
    const [ data, setData ] = React.useState([]);

    const loadData = async () => {
      const response = await fetch('http://localhost:5000/find', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({"limit": 1000})
      });
      const results = await response.json();
      let tmpData = []
      for(let row of results) {
        if(tmpData.length <= row.age){
          while(tmpData.length <= row.age){
            tmpData.push({
              name: tmpData.length,
              Persons: 0,
            })
          }
        }
        tmpData[row.age]['Persons'] += 1;
      }
      setData(tmpData);
    };

    useEffect(() => {
      loadData()
    }, [])

    return (
      <>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <h1>Age distribution for entire dataset</h1>
          </Grid>
        </Grid>
        <Grid container spacing={2}  direction="row" justifyContent="center">
          <Grid item >
          {data.length === 0 ?
            <CircularProgress />
            :
            <BarChart
              width={1000}
              height={300}
              data={data}
            >
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="name" />
              <YAxis />
              <Tooltip />
              <Legend />
              <Brush dataKey="name" height={30} stroke="#8884d8" />
              <Bar dataKey="Persons" barSize={20} fill="#8884d8" />
            </BarChart>
            }
          </Grid>
        </Grid>
      </>
        
  );
}

export default AgeGraph;